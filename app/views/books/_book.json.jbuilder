json.extract! book, :id, :title, :author, :position
json.url book_url(book, format: :json)
