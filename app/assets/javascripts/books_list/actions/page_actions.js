import * as PC from '../constants/page_constants'

export function goToPage (page) {
  return { type: PC.GO_TO_PAGE, page }
}
