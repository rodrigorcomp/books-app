import * as LC from '../constants/loading_constants'

const initialState = true

const loadingReducer = (state = initialState, action) => {
  switch(action.type) {
    case LC.START_LOADING:
      return true;
    case LC.STOP_LOADING:
      return false
    default:
      return state
  }
}

export default loadingReducer
