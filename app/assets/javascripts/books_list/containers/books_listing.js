'use strict';

import React from 'react';
import { connect } from 'react-redux'

import * as pageActions from '../actions/page_actions'
import * as PC from '../constants/page_constants'

import BooksList from '../components/books_list'
import BookItem from '../containers/book_item'

const mapStateToProps = (state) => ({
  items: state.books.map((book, idx) => <BookItem {...book} index={idx+1} />),
  isLoading: state.loading
})

const mapDispatchToProps = (dispach) => {
  return {
    onAddBook: () => dispach(pageActions.goToPage(PC.FORM_IDENTIFIER))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BooksList)
