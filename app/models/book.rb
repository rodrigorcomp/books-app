class Book < ActiveRecord::Base
  default_scope { order(position: :asc) }
  validates_presence_of :title, :author
  acts_as_list add_new_at: :bottom
end
