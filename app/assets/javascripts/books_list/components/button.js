'use strict';

import React from 'react'
import classNames from 'classnames'

const Button = ({children, primary, danger, onClick}) => {

  const buttonClasses = classNames('btn', {
    'btn-primary': primary,
    'btn-danger': danger,
    'btn-default': !primary && !danger
  })

  return (
    <button className={buttonClasses} onClick={onClick}>
      {children}
    </button>
  )
}


export default Button
