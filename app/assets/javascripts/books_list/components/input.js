'use strict';

import React from 'react'
import classNames from 'classnames'

const Input = ({attributes, errors, name, onChange, label }) => {
  let helpBlock = null;

  if (errors[name]) {
    helpBlock = <span className='help-block'>{errors[name]}</span>;
  }

  return (
    <div className={classNames('form-group', { 'has-error': helpBlock })}>
      <label>{label}</label>
      <input type='text'
        className='form-control'
        value={attributes[name]}
        onChange={ ({target}) => onChange(name, target.value) } />
      {helpBlock}
    </div>
  );
}


export default Input
