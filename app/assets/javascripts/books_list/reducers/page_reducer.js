import * as PC from '../constants/page_constants'

const initialState = 'list'

const pageReducer = (state = initialState, action) => {
  switch(action.type) {
    case PC.GO_TO_PAGE:
      return action.page
    default:
      return state
  }
}

export default pageReducer
