'use strict';

import React from 'react'

import Input from './input'
import Button from './button'
import Panel from './panel'

const BooksForm = ({onAttrChange, attributes, errors, onSubmit, onCancel, isLoading}) => (
  <div className='row'>
    <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <Panel title='New Book'
             loading={isLoading}
             footer={
              <div className='btn-group'>
                <Button primary={true} onClick={onSubmit}>Submit</Button>
                <Button danger={true} onClick={onCancel}>Cancel</Button>
              </div>
             }>
        <form>
          <Input
            name='title'
            label='Title'
            onChange={onAttrChange}
            attributes={attributes}
            errors={errors} />

          <Input
            name='author'
            label='Author'
            onChange={onAttrChange}
            attributes={attributes}
            errors={errors} />
        </form>
      </Panel>
    </div>
  </div>
)

export default BooksForm
