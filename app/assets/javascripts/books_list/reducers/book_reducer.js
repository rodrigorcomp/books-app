import * as BKC from '../constants/book_constants'

const initialState = {
  attributes: {
    title: '',
    author: ''
  },
  errors: {}
}

const bookReducer = (state = initialState, action) => {
  const { attributes, errors } = state

  switch(action.type) {
    case BKC.UPDATE_ATTRIBUTE:
      return {
        attributes: Object.assign({}, attributes,
          {
            [action.attributeName]: action.attributeValue
          }
        ),
        errors
      }
    case BKC.ASSIGN_ERRORS:
      return {
        errors: action.errors,
        attributes
      }
    case BKC.RESET_BOOK:
      return initialState;
    default:
      return state
  }
}


export default bookReducer
