import * as BC from '../constants/books_constants'

import * as loadingActions from './loading_actions'

export function appendBook(book) {
  return { type: BC.APPEND_BOOK, book }
}

export function updateBooks(books) {
  return { type: BC.UPDATE_BOOKS, books }
}

export function removeBook(id) {
  return { type: BC.REMOVE_BOOK, id }
}

export function loadBooks() {
  return (dispatch) => {
    dispatch(loadingActions.startLoading())

    $.getJSON('/books', (books) => {
      dispatch(updateBooks(books))
      dispatch(loadingActions.stopLoading())
    })
  }
}

