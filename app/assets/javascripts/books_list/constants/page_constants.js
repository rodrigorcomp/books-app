// actions
export const GO_TO_PAGE = 'GO_TO_PAGE'

// identifiers
export const FORM_IDENTIFIER = 'form'
export const LIST_IDENTIFIER = 'list'
