'use strict';

import { createStore, combineReducers, applyMiddleware, hashHistory } from 'redux'
import ReduxThunk from 'redux-thunk'

import pageReducer from '../reducers/page_reducer'
import booksReducer from '../reducers/books_reducer'
import bookReducer from '../reducers/book_reducer'
import loadingReducer from '../reducers/loading_reducer'

export default function buildStore(preloadedState) {

  const combinedReducers = combineReducers({
    page: pageReducer,
    books: booksReducer,
    newBook: bookReducer,
    loading: loadingReducer
  })

  return createStore(
    combinedReducers,
    preloadedState,
    applyMiddleware(ReduxThunk))
}
