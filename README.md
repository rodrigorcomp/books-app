# Books App

## Dependencies

- Nodejs
- NPM
- bundler
- SQLite
- RVM (or anotther Ruby Version Manager)

## Build

```
git clone git@gitlab.com:rodrigorcomp/books-app.git

cd books-app

bundle install

npm install

cp config/database.example.yml config/database.yml

rake db:create
rake db:migrate

rails server

```


## BooksApp Architecture Overview

### Views Hierarch

`+` are components

`*` are containers

```
+ BooksApp
  * BooksAppBody
    * BooksListing
      + BooksList
        + Panel
          + ListGroup
            * BookItem
              + BookTile
          + Button (new book)
    * BookCreator
      + BookForm
        + Panel
          + Input (title)
          + Input (author)
          + Button (submit)
          + Button (cancel)
```

### State

```javascript
{
  page: String,
  books: [{
    id: Number,
    title: String,
    author: String,
  }],
  newBook: {
    attributes: {
      title: String,
      author: String
    },
    errors: {
      title: String,
      author: String
    }
  },
  loading: Boolean
}
```
