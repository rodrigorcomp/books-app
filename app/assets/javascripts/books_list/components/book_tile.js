'use strict';

import React from 'react'
import Button from './button'

const BookTile = ({index, id, title, author, onRemove, onMoveUp, onMoveDown}) => (
  <div className='row'>
    <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
      <h4 className="list-group-item-heading"> {index} - {title} </h4>
      <p className="list-group-item-text"> {author} </p>
    </div>

    <div className='col-lg-6 col-md-6 col-sm-12 col-xs-12'>
      <div className="btn-group pull-right" role="group">
        <Button danger={true} onClick={() => onRemove(id)}>
          Remove
        </Button>
        <Button primary={true} onClick={() => onMoveUp(id)}>
          <i className='glyphicon glyphicon-arrow-up'></i>
        </Button>
        <Button primary={true} onClick={() => onMoveDown(id)}>
          <i className='glyphicon glyphicon-arrow-down'></i>
        </Button>
      </div>
    </div>
  </div>
)

export default BookTile
