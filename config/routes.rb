Rails.application.routes.draw do
  resources :books, except: [:show, :edit] do
    member do
      put :move_up
      put :move_down
    end
  end

  root to: 'books#index'
end
