import * as BKC from '../constants/book_constants'
import * as PC from '../constants/page_constants'

import * as booksActions from './books_actions'
import * as loadingActions from './loading_actions'
import * as pageActions from './page_actions'

export function updateAttribute (attributeName, attributeValue) {
  return { type: BKC.UPDATE_ATTRIBUTE, attributeName, attributeValue }
}

export function assignErrors(errors) {
  return { type: BKC.ASSIGN_ERRORS, errors }
}

export function resetBook() {
  return { type: BKC.RESET_BOOK }
}

export function submitBook () {
  return (dispatch, getState) => {
    dispatch(loadingActions.startLoading())
    const book = getState().newBook.attributes;

    const onBookCreated = (book) => {
      dispatch(booksActions.appendBook(book))
      dispatch(pageActions.goToPage(PC.LIST_IDENTIFIER))
      dispatch(resetBook())
      dispatch(loadingActions.stopLoading())
    }

    const onFailure = ({responseJSON}) => {
      dispatch(assignErrors(responseJSON))
      dispatch(loadingActions.stopLoading())
    }

    $.post('/books', { book }, onBookCreated, 'json')
      .fail(onFailure);
  }
}

export function deleteBook(id) {
  return (dispatch) => {
    dispatch(loadingActions.startLoading())

    const onBookRemoved = () => {
      dispatch(booksActions.removeBook(id));
      dispatch(loadingActions.stopLoading())
    }

    $.post("books/" + id, { _method: 'delete' }, onBookRemoved, 'json')
  }
}

export function moveUp (bookId) {
  return (dispatch) => {
    dispatch(loadingActions.startLoading())
    $.ajax({
      url: '/books/' + bookId + '/move_up',
      type: 'PUT',
      dataType: 'json',
      success: (books) => {
        dispatch(booksActions.updateBooks(books))
        dispatch(loadingActions.stopLoading())
      }
    })
  }
}

export function moveDown (bookId) {
  return (dispatch) => {
    dispatch(loadingActions.startLoading())
    $.ajax({
      url: '/books/' + bookId + '/move_down',
      type: 'PUT',
      dataType: 'json',
      success: (books) => {
        dispatch(booksActions.updateBooks(books))
        dispatch(loadingActions.stopLoading())
      }
    })
  }
}
