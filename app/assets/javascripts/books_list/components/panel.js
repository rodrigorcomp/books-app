'use strict';

import React from 'react'

import classNames from 'classnames'

const Panel = ({title, children, footer, loading}) => (
  <div className="panel panel-default">
    <div className="panel-heading">
      <h3 className="panel-title">
        {title}
        <span className={classNames('glyphicon glyphicon-refresh spin pull-right', { loading: loading })}></span>
      </h3>
    </div>
    <div className="panel-body">
      {children}
    </div>
    <div className="panel-footer">
      {footer}
    </div>
  </div>
)


export default Panel
