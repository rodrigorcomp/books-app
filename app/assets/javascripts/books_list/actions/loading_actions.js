import * as LC from '../constants/loading_constants'

export function startLoading() {
  return { type: LC.START_LOADING }
}

export function stopLoading() {
  return { type: LC.STOP_LOADING }
}
