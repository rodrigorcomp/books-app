'use strict';

import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'

import BooksApp from './components/books_app'
import buildStore from './store/store'

import * as booksActions from './actions/books_actions'

const runBooksListApp = (booksRoot) => {

  const store = buildStore();

  ReactDOM.render(
    <Provider store={store}>
      <BooksApp />
    </Provider>, booksRoot);

  store.dispatch(booksActions.loadBooks())
}

export { runBooksListApp };
