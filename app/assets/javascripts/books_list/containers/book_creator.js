'use strict';

import React from 'react'
import { connect } from 'react-redux'

import * as bookActions from '../actions/book_actions'
import * as pageActions from '../actions/page_actions'

import * as PC from '../constants/page_constants'

import BookForm from '../components/book_form'

const mapStateToProps = (state) => ({
  attributes: state.newBook.attributes,
  errors: state.newBook.errors,
  isLoading: state.loading
})

const mapDispatchToProps = (dispatch) => ({
  onAttrChange: (attrName, attrValue) => {
    dispatch(bookActions.updateAttribute(attrName, attrValue))
  },
  onSubmit: () => dispatch(bookActions.submitBook()),
  onCancel: () => dispatch(pageActions.goToPage(PC.LIST_IDENTIFIER))
})

export default connect(mapStateToProps, mapDispatchToProps)(BookForm)
