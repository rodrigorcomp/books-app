'use strict';

import React from 'react'
import { connect } from 'react-redux'

import * as bookActions from '../actions/book_actions'
import * as pageActions from '../actions/page_actions'

import * as PC from '../constants/page_constants'

import BookTile from '../components/book_tile'

const mapDispatchToProps = (dispatch) => ({
  onRemove: (id) => dispatch(bookActions.deleteBook(id)),
  onMoveUp: (id) => dispatch(bookActions.moveUp(id)),
  onMoveDown: (id) => dispatch(bookActions.moveDown(id))
})

export default connect(null, mapDispatchToProps)(BookTile)
