'use strict';

import React from 'react'

import Button from './button'
import ListGroup from './list_group'
import Panel from './panel'

const BooksList = ({ onAddBook, items, isLoading }) => (
  <div className='row'>
    <div className='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <Panel title='Books List'
             loading={isLoading}
             footer={<Button onClick={onAddBook}>New Book</Button>}>
        <ListGroup items={items} />
      </Panel>
    </div>
  </div>
)

export default BooksList
