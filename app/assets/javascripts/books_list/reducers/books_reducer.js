import * as BC from '../constants/books_constants'

const initialState = []

const booksReducer = (state = initialState, action) => {
  switch(action.type) {
    case BC.UPDATE_BOOKS:
      return action.books;
    case BC.APPEND_BOOK:
      return [...state, action.book]
    case BC.REMOVE_BOOK:
      return state.filter((book) => book.id != action.id)
    default:
      return state
  }
}

export default booksReducer
