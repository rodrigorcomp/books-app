var runBooksListApp = require('./books_list/index').runBooksListApp;


$(function () {
  var booksRoot = document.getElementById('books-root');

  if (booksRoot) {
    runBooksListApp(booksRoot);
  }
});

