'use strict';

import React from 'react';
import { connect } from 'react-redux'

import BookCreator from '../containers/book_creator'
import BooksListing from '../containers/books_listing'

import * as PC from '../constants/page_constants'

const pagesMap = {
  [PC.LIST_IDENTIFIER]: <BooksListing />,
  [PC.FORM_IDENTIFIER]: <BookCreator />
}

const mapStateToProps = (state) => ({ currentPage: state.page })

// renders component based on current page
const BooksAppBody = ({currentPage}) => pagesMap[currentPage]

export default connect(mapStateToProps)(BooksAppBody)
