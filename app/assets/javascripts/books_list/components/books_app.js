'use strict';

import React from 'react'

import BooksAppBody from '../containers/books_app_body'

const BooksApp = () => (
  <div className='container books-app'>
    <div className='page-header'>
      <h1>Books App</h1>
    </div>

    <BooksAppBody />
  </div>
)

BooksApp.contextTypes = {
  store: React.PropTypes.object
};

export default BooksApp
