'use strict';

import React from 'react'

const ListGroup = ({items}) => (
  <ul className='list-group'>
    { items.map((item, i) => (
        <li className='list-group-item' key={i}>
          {item}
        </li>
      )
    ) }
  </ul>
)

export default ListGroup
